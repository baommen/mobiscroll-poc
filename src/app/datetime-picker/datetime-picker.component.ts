import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import * as moment from 'moment-timezone';
import { MbscDatepicker, momentTimezone } from '@mobiscroll/angular';
import { Platform } from '@angular/cdk/platform';

momentTimezone.moment = moment;

export interface DatetimePickerChangeEvent {
  startDate: Date;
  endDate?: Date;
  timeZone?: string;
}

export class DatetimePickerConfig {
  startDate: Date = new Date();
  endDate?: Date;
  timeZone?: string = DEFAULT_TIMEZONE;
  maxFutureDays?: number = DEFAULT_MAX_FUTURE_DAYS;
  stepMinutes?: number = DEFAULT_STEP_MINUTES;
  minDurationMinutes?: number = DEFAULT_MIN_DURATION_MINUTES;
}

export type Theme = 'ios' | 'material' | 'windows';
const DEFAULT_THEME: Theme = 'ios';

export type ThemeVariant = 'light' | 'dark';
const DEFAULT_THEME_VARIANT: ThemeVariant = 'light';

const DEFAULT_MAX_FUTURE_DAYS: number = 5;
const DEFAULT_STEP_MINUTES: number = 5;
const DEFAULT_MIN_DURATION_MINUTES: number = 30;
const DEFAULT_TIMEZONE: string = 'UTC';

export type DatetimePickerSelector = 'start' | 'end';

@Component({
  selector: 'app-datetime-picker',
  templateUrl: './datetime-picker.component.html',
  styleUrls: ['./datetime-picker.component.scss'],
})
export class DatetimePickerComponent {
  public now = new Date();
  public momentPlugin = momentTimezone;

  @Output() onValueChange: EventEmitter<DatetimePickerChangeEvent> = new EventEmitter<DatetimePickerChangeEvent>();

  @ViewChild(MbscDatepicker) datePicker!: MbscDatepicker;
  private startDate: Date = this.now;
  private endDate?: Date;
  private value: [Date, Date?] = [new Date(), undefined];
  private themeVariant: ThemeVariant = DEFAULT_THEME_VARIANT;
  private minDurationMinutes: number = DEFAULT_MIN_DURATION_MINUTES;
  private timeZone: string = DEFAULT_TIMEZONE;
  private stepMinute: number = DEFAULT_STEP_MINUTES;

  // Temp fields
  private tempStart?: Date = this.now;
  private tempEnd?: Date;
  private tempDuration?: number;

  constructor(private platform: Platform) {}

  public get getTheme(): Theme {
    if (this.platform.IOS) {
      return 'ios';
    } else if (this.platform.ANDROID) {
      return 'material';
    }
    return DEFAULT_THEME;
  }

  public get getThemeVariant(): ThemeVariant {
    return this.themeVariant;
  }

  public get getTimezone(): string {
    return this.timeZone;
  }

  public get getStepMinute(): number {
    return this.stepMinute;
  }

  /**
   * Get the minimum date of the datepicker, rounded to the next time interval.
   * @return ISO string value of min date
   */
  public get getMinDate(): string {
    const start = moment(this.startDate).tz(this.timeZone);
    let minutesToNextInterval = this.stepMinute - (start.minute() % this.stepMinute);
    // Prevent current time to be rounded to next interval
    minutesToNextInterval = minutesToNextInterval < this.stepMinute ? minutesToNextInterval : 0;
    return start.add(minutesToNextInterval, 'minutes').toISOString();
  }

  /**
   * Get the maximum date of the datepicker
   * @return ISO string value of max date
   */
  public get getMaxDate(): string {
    const start = moment(this.getMinDate).tz(this.timeZone);
    return start.add(DEFAULT_MAX_FUTURE_DAYS, 'days').toISOString();
  }

  /**
   * Return the minimum range for the datepicker in milliseconds
   */
  public get getMinRange() {
    let minRange = this.minDurationMinutes;

    // Possible method to allow for booking the remainder of the next timeslot
    // If the start date is before the minimum date, then adjust the minimum range to allow partial bookings
    // For example from 11:04 to 12:00, when the minimum duration is 60 minutes
    const minDate = moment(this.getMinDate);
    if (minDate.diff(moment(this.startDate), 'minutes') > 0) {
      const minRangeAdjustment = minDate.diff(moment(this.startDate), 'minutes');
      // The adjusted duration cannot be less than half the actual minimum duration
      minRange = minRangeAdjustment < this.stepMinute / 2 ? minRange - minRangeAdjustment : minRange;
    }

    return minRange * 60 * 1000;
  }

  /**
   * Initializes the values of the datepicker
   * @param config The configuration object
   */
  public initialize(config: DatetimePickerConfig) {
    // set values to datepicker
    this.startDate = config.startDate;
    this.endDate = config.endDate;
    if (moment.tz.zone(config.timeZone ?? '') == null) {
      throw new Error('Invalid timezone provided');
    }
    this.timeZone = config.timeZone ?? DEFAULT_TIMEZONE;
    this.stepMinute = config.stepMinutes ?? DEFAULT_STEP_MINUTES;
    this.minDurationMinutes = config.minDurationMinutes ?? DEFAULT_MIN_DURATION_MINUTES;

    this.value = [config.startDate, config.endDate];
    this.datePicker.setVal(this.value);
  }

  /**
   * Triggered by the change event of the datepicker
   * Emits change event to parent component with datepicker values
   * @param aEvent
   */
  public onChange(aEvent: any): void {
    // Check if both values have been set
    if (aEvent.value && aEvent.value[0] && aEvent.value[1]) {
      // Emit change event
      this.onValueChange.emit({
        startDate: moment(aEvent.value[0]).tz(this.timeZone).startOf('minute').toDate(),
        endDate: moment(aEvent.value[1]).tz(this.timeZone).startOf('minute').toDate() ?? null,
        timeZone: this.datePicker.dataTimezone,
      });
      this.startDate = aEvent.value[0];
      this.endDate = aEvent.value[1];
    }
  }

  /**
   * Compares the start and end date of the range picker and makes sure that the original duration is maintained when the start date changes
   * @param aEvent
   */
  public onTempValueChange(aEvent: { inst: MbscDatepicker; value: [Date, Date] }): void {
    const previousTempStartDate = this.tempStart;
    const previousTempDuration = this.tempDuration;
    // Set the new temporary values
    this.tempStart = aEvent.value[0];
    this.tempEnd = aEvent.value[1];

    // Check if the start and enddate have been set, then calculate duration
    if (this.tempStart && this.tempEnd) {
      this.tempDuration = this.dateDifferenceInMinutes(this.tempStart, this.tempEnd);
    }
    /// Check if a duration has been previously set, and check if the start date has changed, then re-calculate the end date
    if (
      previousTempDuration &&
      previousTempStartDate &&
      this.dateDifferenceInMinutes(this.tempStart, previousTempStartDate) !== 0
    ) {
      this.tempEnd = this.addMinutes(this.tempStart, previousTempDuration);
      // Reset the duration to the previous duration
      this.tempDuration = previousTempDuration;
      // Force the temporary values in the date time picker
      aEvent.inst.setTempVal([this.tempStart, this.tempEnd]);
    }
  }

  public dateDifferenceInMinutes(date1: Date, date2: Date): number {
    return (date2.getTime() - date1.getTime()) / 60 / 1000;
  }

  /**
   * Adds minutes to a date
   * @param date
   * @param minutes
   */
  public addMinutes(date: Date, minutes: number): Date {
    return new Date(date.getTime() + minutes * 60 * 1000);
  }

  /**
   * Opens the datepicker component
   */
  public open(selector: DatetimePickerSelector = 'start'): void {
    this.datePicker.setActiveDate(selector);
    this.datePicker.open();
  }
}
