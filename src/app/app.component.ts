import { Component, ViewChild } from '@angular/core';
import {
  DatetimePickerSelector,
  DatetimePickerChangeEvent,
  DatetimePickerComponent,
} from './datetime-picker/datetime-picker.component';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  startDateFormControl: FormControl = new FormControl();
  endDateFormControl: FormControl = new FormControl();

  @ViewChild(DatetimePickerComponent) picker!: DatetimePickerComponent;

  /**
   * Triggered by the datetime pickers onValueChange event.
   * @param event
   */
  onValueChange(event: DatetimePickerChangeEvent): void {
    // This would be the place to manipulate the date-time fields before storing them in the form values
    this.startDateFormControl.setValue(event.startDate);
    this.endDateFormControl.setValue(event.endDate);
  }

  /**
   * Initializes the picker with the values from the formcontrol and opens it.
   */
  openPicker(selector: DatetimePickerSelector = 'start'): void {
    // This would be the place to manipulate the date-time values before initializing the picker
    this.picker.initialize({
      startDate: this.startDateFormControl.value ?? new Date(),
      endDate: this.endDateFormControl.value,
      timeZone: 'UTC',
      maxFutureDays: 10,
      minDurationMinutes: 60,
      stepMinutes: 5,
    });
    this.picker.open(selector);
  }
}
