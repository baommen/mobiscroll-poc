# Mobiscroll Date & time picker POC

Proof of concept for the integration of the [Mobiscroll Date & time picker](https://mobiscroll.com/date-time-picker-calendar) into the WEA Live App.
This project uses a trial version of the Mobiscroll component. Usage is limited for 10 days, starting on April 11th 2022. For usage beyond the duration of the trial license, a new account will need to be created at mobiscroll.com, and the steps to install the component will need to be followed again. This will override the current license.

The project has been setup outside the regular Planon project setup, due to the license described above. It was not possible to integrate an external authenticated NPM-repository in our builds.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
